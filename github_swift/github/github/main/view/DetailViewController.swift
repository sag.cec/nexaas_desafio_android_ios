//
//  DetailViewController.swift
//  github
//
//  Created by Gutemberg Albuquerque Da Silva on 07/08/18.
//  Copyright © 2018 Gutemberg Albuquerque Da Silva. All rights reserved.
//

import UIKit

class DetailViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvListId : UITableView!;
    
    var array : [String : Any]!;

    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.start();
    }
    
    override func viewWillAppear(_ animated : Bool) {
        super.viewWillAppear(animated);
        
        self.fill();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
    }
    
    func tableView(_ tableView : UITableView, numberOfRowsInSection section : Int) -> Int {
        return self.array.count;
    }
    
    func tableView(_ tableView : UITableView, cellForRowAt indexPath : IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier : "cellId") as! LinhaDetailTableViewCell;
        
        return cell;
    }
    
    internal func start() {
        self.tvListId.register(UINib(nibName : "LinhaDetailTableViewCell", bundle : nil), forCellReuseIdentifier : "cellId");
        
        self.array = [String : Any]();
    }
    
    internal func fill() {
        self.navigationItem.title = self.array["name"] as? String;
    }

}
