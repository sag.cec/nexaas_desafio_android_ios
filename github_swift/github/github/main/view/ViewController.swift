//
//  ViewController.swift
//  github
//
//  Created by Gutemberg Albuquerque Da Silva on 06/08/18.
//  Copyright © 2018 Gutemberg Albuquerque Da Silva. All rights reserved.
//

import UIKit

class ViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvListId : UITableView!;
    
    var array : [[String : Any]]!;

    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.start();
    }
    
    override func viewWillAppear(_ animated : Bool) {
        super.viewWillAppear(animated);
        
        self.fill();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        
        self.fill();
    }
    
    override func prepare(for segue : UIStoryboardSegue, sender : Any?) {
        if let s = segue.destination as? DetailViewController {
            if let index = self.tvListId.indexPathForSelectedRow {
                s.array = self.array[index.row];
            }
        }
    }
    
    func tableView(_ tableView : UITableView, numberOfRowsInSection section : Int) -> Int {
        return self.array?.count ?? 0;
    }
    
    func tableView(_ tableView : UITableView, cellForRowAt indexPath : IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier : "cellId", for : indexPath) as! LinhaTableViewCell;
        
        let array1 = self.array[indexPath.row];
        let array2 = array1["owner"] as! [String : Any];
        
        cell.lRepositorioTitleId.text = array1["name"] as? String;
        cell.lRepositorioDescriptionId.text = array1["description"] as? String;
        cell.lRepositorioUsernameId.text = array2["login"] as? String;
        cell.lRepositorioFullnameId.text = array1["full_name"] as? String;
        
        cell.lRepositorioForkId.text = array1["forks_count"] as? String ?? "000";
        cell.lRepositorioStarId.text = array1["stargazers_count"] as? String ?? "000";
        
        return cell;
    }
    
    func tableView(_ tableView : UITableView, didSelectRowAt indexPath : IndexPath) {
        self.performSegue(withIdentifier : "segueDetailViewController", sender : self);
    }
    
    internal func start() {
        self.tvListId.register(UINib(nibName : "LinhaTableViewCell", bundle : nil), forCellReuseIdentifier : "cellId");
        
        self.array = [[String : Any]]();
    }
    
    internal func fill() {
        let mur = RepositorioMur();
        mur.doBackground { (_ ok : Bool, _ array : [[String : Any]]?) in
            if (ok) {
                guard array != nil else {
                    return;
                }
            }
            
            self.array = array;
            
            self.tvListId.reloadData();
        }
    }

}
