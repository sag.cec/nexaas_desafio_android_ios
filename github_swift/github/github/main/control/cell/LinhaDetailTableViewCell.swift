//
//  LinhaTableViewCell.swift
//  github
//
//  Created by Gutemberg Albuquerque Da Silva on 06/08/18.
//  Copyright © 2018 Gutemberg Albuquerque Da Silva. All rights reserved.
//

import UIKit

class LinhaDetailTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lRepositorioTitleId : UILabel!;
    
    @IBOutlet weak var lRepositorioDescriptionId : UILabel!;
    
    @IBOutlet weak var lRepositorioUsernameId : UILabel!;
    
    @IBOutlet weak var lRepositorioFullnameId : UILabel!;
    
    override func awakeFromNib() {
        super.awakeFromNib();
    }

    override func setSelected(_ selected : Bool, animated : Bool) {
        super.setSelected(selected, animated : animated);
    }
    
}
