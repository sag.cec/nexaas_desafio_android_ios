//
//  RepositorioMur.swift
//  github
//
//  Created by Gutemberg Albuquerque Da Silva on 07/08/18.
//  Copyright © 2018 Gutemberg Albuquerque Da Silva. All rights reserved.
//

import Foundation

class RepositorioMur {
    
    /**
     * DEFAULT
     */
    
    let url : URL!;
    
    let nmuRequest : NSMutableURLRequest!;
    
    /**
     * CONSTRUCT
     */
    
    init() {
        self.url = URL(string : "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1");
        
        self.nmuRequest = NSMutableURLRequest(url : self.url!);
    }
    
    /**
     *
     */
    
    func doBackground(completion : @escaping (_ ok : Bool, _ array : [[String : Any]]?) -> Void) {
        self.nmuRequest.httpMethod = "GET";
        self.nmuRequest.addValue("application/json", forHTTPHeaderField : "Content-Type");
        self.nmuRequest.addValue("application/json", forHTTPHeaderField : "Accept");
        
        URLSession.shared.dataTask(with : self.nmuRequest as URLRequest) {
            data, response, error in
            guard
                let data = data,   // unwrap data
                error == nil,      // se nao houver error
                
                (response as? HTTPURLResponse)?.statusCode == 200   // e status code for igual a 200
                
                else {
                    
                    DispatchQueue.main.async {
                        completion(false, nil);
                    }
                    
                    return;
            }
            
            do {
                let jo = try JSONSerialization.jsonObject(with : data) as! [String : Any];
                let array = jo["items"] as? [[String : Any]];
                
                DispatchQueue.main.async {
                    completion(true, array);
                }
            } catch let error as NSError {
                print(error);
                
                DispatchQueue.main.async {
                    completion(false, nil);
                }
            }
            
            }.resume();
    }
    
}
