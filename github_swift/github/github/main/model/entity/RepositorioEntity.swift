//
//  RepositorioEntity.swift
//  github
//
//  Created by Gutemberg Albuquerque Da Silva on 07/08/18.
//  Copyright © 2018 Gutemberg Albuquerque Da Silva. All rights reserved.
//

import Foundation

class RepositorioEntity  : NSObject, NSCoding, Codable {
    
    var id : Int64?;
    
    var denominacao : String?;
    
    var descricao : String?;
    
    var descricaoMae : String?;
    
    var descricaoPai : String?;
    
    var observacao : String?;
    
    //
    
    var imagem : String?;
    
    //
    
    override init() {
        
    }
    
    init(id : Int64, denominacao : String, descricao : String, observacao : String) {
        self.id = id;
        self.denominacao = denominacao.trimmingCharacters(in : .whitespacesAndNewlines).uppercased();
        self.descricao = descricao.trimmingCharacters(in : .whitespacesAndNewlines).uppercased();
        self.observacao = observacao.trimmingCharacters(in : .whitespacesAndNewlines).uppercased();
    }
    
    //
    
    required convenience init(coder aDecoder : NSCoder) {
        self.init();
        
        self.id = aDecoder.decodeObject(forKey : "id") as? Int64;
        self.denominacao = (aDecoder.decodeObject(forKey : "denominacao") as? String)?.uppercased();
        self.descricao = (aDecoder.decodeObject(forKey : "descricao") as? String)?.uppercased();
        self.descricaoMae = (aDecoder.decodeObject(forKey : "descricaoMae") as? String)?.uppercased();
        self.descricaoPai = (aDecoder.decodeObject(forKey : "descricaoPai") as? String)?.uppercased();
        self.observacao = (aDecoder.decodeObject(forKey : "observacao") as? String)?.uppercased();
        self.imagem = aDecoder.decodeObject(forKey : "imagem") as? String;
    }
    
    func encode(with aCoder : NSCoder) {
        aCoder.encode(self.id, forKey : "id");
        aCoder.encode(self.denominacao, forKey : "denominacao");
        aCoder.encode(self.descricao, forKey : "descricao");
        aCoder.encode(self.descricaoMae, forKey : "descricaoMae");
        aCoder.encode(self.descricaoPai, forKey : "descricaoPai");
        aCoder.encode(self.observacao, forKey : "observacao");
        aCoder.encode(self.imagem, forKey : "imagem");
    }
    
    /**
     *
     */
    
    func json() -> [String : Any] {
        let jo = ["id" : self.id ?? NSNull(),
                  "denominacao" : self.denominacao!.trimmingCharacters(in : .whitespacesAndNewlines).uppercased(),
                  "descricao" : self.descricao!.trimmingCharacters(in : .whitespacesAndNewlines).uppercased(),
                  "descricaoMae" : self.descricaoMae!.trimmingCharacters(in : .whitespacesAndNewlines).uppercased(),
                  "descricaoPai" : self.descricaoPai!.trimmingCharacters(in : .whitespacesAndNewlines).uppercased(),
                  "observacao" : self.observacao!.trimmingCharacters(in : .whitespacesAndNewlines)] as [String : Any];
        
        return jo;
    }
    
    func encode() -> Data {
        let encoder = JSONEncoder();
        encoder.outputFormatting = .prettyPrinted;
        
        let data = try! encoder.encode(self);
        
        //print(String(data : data, encoding : .utf8)!);
        
        return data;
    }
    
}
