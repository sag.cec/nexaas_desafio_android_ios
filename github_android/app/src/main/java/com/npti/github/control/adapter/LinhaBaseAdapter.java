package com.npti.github.control.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.npti.github.R;
import com.npti.github.model.entity.ItemEntity;

import org.json.JSONObject;

import java.util.List;

public class LinhaBaseAdapter extends BaseAdapter {

    private static LayoutInflater li;

    private Context context;

    private List<ItemEntity> list;

    public LinhaBaseAdapter(Context c, List<ItemEntity> list) {
        this.context = c;
        this.list = list;

        li = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public ItemEntity getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if (v == null) {
            v = li.inflate(R.layout.layout_row, null);
        }

        try {
            ItemEntity ent = (ItemEntity) this.list.get(i);

            TextView tvTituloRepositorioId = v.findViewById(R.id.tvTituloRepositorioId);
            tvTituloRepositorioId.setText(ent.getName());

            TextView tvDescricaoRepositorioId = v.findViewById(R.id.tvDescricaoRepositorioId);
            tvDescricaoRepositorioId.setText(ent.getDescription());

            TextView tvForksCountId = v.findViewById(R.id.tvForksCountId);
            tvForksCountId.setText(ent.getForksCount());

            TextView tvStargazersCountId = v.findViewById(R.id.tvStargazersCountId);
            tvStargazersCountId.setText(ent.getStargazersCount());

            TextView tvUsernameId = v.findViewById(R.id.tvUsernameId);
            tvUsernameId.setText(ent.getOwner().getLogin());

            TextView tvNomeSobrenomeId = v.findViewById(R.id.tvNomeSobrenomeId);
            tvNomeSobrenomeId.setText(ent.getFullName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

}