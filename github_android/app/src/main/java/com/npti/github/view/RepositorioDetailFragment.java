package com.npti.github.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.npti.github.R;
import com.npti.github.control.adapter.LinhaDetailBaseAdapter;
import com.npti.github.model.entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RepositorioDetailFragment extends Fragment {

    private ListView lvListId;

    private TextView tvForksCountId;

    private TextView tvStargazersCountId;

    private ItemEntity entity;

    public RepositorioDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = this.getArguments();
        this.entity = (ItemEntity) b.getSerializable("ent");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout_row for this fragment
        View v = inflater.inflate(R.layout.fragment_repositorio_detail, container, false);

        this.create(v);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        this.start();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.fill();
    }

    private void create(View v) {
        this.lvListId = v.findViewById(R.id.lvListId);

        this.tvForksCountId = v.findViewById(R.id.tvForksCountId);
        this.tvStargazersCountId = v.findViewById(R.id.tvStargazersCountId);
    }

    private void start() {
        List list = new ArrayList();
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");

        this.lvListId.setAdapter(new LinhaDetailBaseAdapter(super.getContext(), list));
    }

    private void fill() {
        this.tvForksCountId.setText(this.entity.getForksCount());
        this.tvStargazersCountId.setText(this.entity.getStargazersCount());
    }

}