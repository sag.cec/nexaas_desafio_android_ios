package com.npti.github.model.entity;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class OwnerEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    @SerializedName("login")
    private String login;

    /**
     * REFERENCIA - FK
     */

    /**
     * REFERENCIA - LIST
     */

    /**
     * TRANSIENT
     */

    /**
     * CONSTRUTOR
     */

    public OwnerEntity() {
        // TODO Auto-generated constructor stub
    }

    public OwnerEntity(Long id) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    public OwnerEntity(Long id, Boolean cancelado) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    /**
     * GETS AND SETS
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     */

    /**
     *
     */

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}