package com.npti.github.control.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.npti.github.model.entity.RepositorioEntity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RepositorioListAt extends AsyncTask<Void, Void, Boolean> {

    private Context context;

    private String path = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";

    private HttpURLConnection conn;

    private RepositorioEntity entity;

    public RepositorioListAt(Context c) {
        this.context = c;
    }

    public RepositorioEntity getEntity() {
        return entity;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            URL url = new URL(this.path);

            this.conn = (HttpURLConnection) url.openConnection();
            this.conn.setRequestMethod("GET");
            this.conn.setConnectTimeout(10000);

            int responseCode = this.conn.getResponseCode();

            if (responseCode != HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader((this.conn.getErrorStream())));
                String s = br.readLine();

                if (responseCode == 403) {
                    throw new IOException();
                } else if (responseCode == 500) {
                    throw new Exception(s);
                } else {
                    throw new RuntimeException();
                }
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((this.conn.getInputStream())));

            String output;

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            while ((output = br.readLine()) != null) {
                this.entity = gson.fromJson(output, RepositorioEntity.class);
            }

            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //if (ObjectUtils.isNotBlank(this.conn)) {
            this.conn.disconnect();
            //}
        }

        return Boolean.FALSE;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        try {
            if (result) {

            }
        } catch (Exception e) {
            Toast.makeText(this.context, "Ocorreu falha ao salvar as informações. Por favor," +
                    " realize novamente.", Toast.LENGTH_SHORT).show();

            Log.e(":: [" + this.getClass().getSimpleName() + "]", e.getMessage());
        } finally {

        }
    }

}