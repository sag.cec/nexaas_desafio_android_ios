package com.npti.github.model.entity;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class ItemEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    @SerializedName("description")
    private String description;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("forks_count")
    private String forksCount;

    @SerializedName("stargazers_count")
    private String stargazersCount;

    /**
     * REFERENCIA - FK
     */

    @SerializedName("owner")
    private OwnerEntity owner;

    /**
     * REFERENCIA - LIST
     */

    /**
     * TRANSIENT
     */

    /**
     * CONSTRUTOR
     */

    public ItemEntity() {
        // TODO Auto-generated constructor stub
    }

    public ItemEntity(Long id) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    public ItemEntity(Long id, Boolean cancelado) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    /**
     * GETS AND SETS
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getForksCount() {
        return forksCount;
    }

    public void setForksCount(String forksCount) {
        this.forksCount = forksCount;
    }

    public String getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(String stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public OwnerEntity getOwner() {
        return owner;
    }

    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

    /**
     *
     */

    /**
     *
     */

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}