package com.npti.github.control.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.npti.github.R;

import java.util.List;

public class LinhaDetailBaseAdapter extends BaseAdapter {

    private static LayoutInflater li;

    private Context context;

    private List list;

    public LinhaDetailBaseAdapter(Context c, List list) {
        this.context = c;
        this.list = list;

        li = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if (v == null) {
            v = li.inflate(R.layout.layout_detail_row, null);
        }

        return v;
    }

}