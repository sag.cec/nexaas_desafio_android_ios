package com.npti.github.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.npti.github.R;
import com.npti.github.control.adapter.LinhaBaseAdapter;
import com.npti.github.control.async.RepositorioListAt;
import com.npti.github.model.entity.ItemEntity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RepositorioListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ListView lvListId;

    public RepositorioListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout_row for this fragment
        View v = inflater.inflate(R.layout.fragment_repositorio_list, container, false);

        this.create(v);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        this.start();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.fill();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ItemEntity ent = (ItemEntity) this.lvListId.getItemAtPosition(i);

        Bundle b = new Bundle();
        b.putSerializable("ent", ent);

        RepositorioDetailFragment frag = new RepositorioDetailFragment();
        frag.setArguments(b);

        super.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flContentId, frag).addToBackStack(null).commit();
    }

    private void create(View v) {
        this.lvListId = v.findViewById(R.id.lvListId);
        this.lvListId.setOnItemClickListener(this);
    }

    private void start() {

    }

    private void fill() {
        try {
            RepositorioListAt rla = new RepositorioListAt(super.getContext());
            rla.execute();

            if (rla.get()) {
                this.lvListId.setAdapter(new LinhaBaseAdapter(super.getContext(), rla.getEntity().getItemList()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}