package com.npti.github.model.entity;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class RepositorioEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    @SerializedName("total_count")
    private String totalCount;

    /**
     * REFERENCIA - FK
     */

    /**
     * REFERENCIA - LIST
     */

    @SerializedName("items")
    private List<ItemEntity> itemList;

    /**
     * TRANSIENT
     */

    /**
     * CONSTRUTOR
     */

    public RepositorioEntity() {
        // TODO Auto-generated constructor stub
    }

    public RepositorioEntity(Long id) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    public RepositorioEntity(Long id, Boolean cancelado) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    /**
     * GETS AND SETS
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public List<ItemEntity> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemEntity> itemList) {
        this.itemList = itemList;
    }

    /**
     *
     */

    /**
     *
     */

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}